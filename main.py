from game import Game


game = Game()


if __name__ == "__main__":
    while game.running:
        game.current_menu.display_menu()
        game.game_loop()
