import pygame
import random
from menu import MainMenu, OptionsMenu, CreditsMenu
from ship import Player, Enemy, Explosion
from bonus import Bonus
from constants import (
    FPS, WIDTH, HEIGHT, WHITE, RED, GREEN, MAIN_FONT, LARGE_FONT, BACKGROUND
)


class Game():
    """Class representing a game and handles (detects and executes) events within the game."""
    def __init__(self):
        pygame.init()
        self.running = True
        self.playing = False
        self.up_key = False
        self.down_key = False
        self.start_key = False
        self.escape_key = False
        self.background = pygame.transform.scale(BACKGROUND, (WIDTH, HEIGHT))
        self.display = pygame.Surface((WIDTH, HEIGHT))
        self.window = pygame.display.set_mode((WIDTH, HEIGHT))
        self.font_name = "assets/Starjhol.ttf"
        self.main_menu = MainMenu(self)
        self.options_menu = OptionsMenu(self)
        self.credits_menu = CreditsMenu(self)
        self.current_menu = self.main_menu
        self.keys_pressed = None
        self.player = None
        self.enemies = []
        self.enemies_horde_size = 5
        self.level = 1
        self.bonus = None
        self.explosion_group = pygame.sprite.Group()
        self.clock = pygame.time.Clock()

    @staticmethod
    def collide(obj1, obj2):
        """Static method that checks if a given objects overlap by at least 1 pixel."""
        offset_x = obj2.x - obj1.x
        offset_y = obj2.y - obj1.y

        return obj1.mask.overlap(obj2.mask, (offset_x, offset_y)) != None

    def blit_screen(self):
        """Draws (blit) a canvas (game.display) onto a window (game.window) and update it."""
        self.window.blit(self.display, (0, 0))
        pygame.display.update()

    def game_loop(self):
        "Acts as a main loop and orchestrates a game."
        self.clock.tick(FPS)

        while self.playing:
            self.check_events()
            if self.escape_key:
                self.game_over()
                break

            if self.player.lives <= 0:
                self.game_over()
                break

            if self.player.health <= 0:
                self.player.lives -= 1
                self.player.reset_health()
                self.player.reset_position()

            # self.level_changer()
            self.create_enemies()

            for enemy in self.enemies:
                enemy.move()
                enemy.move_bullets(self.player, self.explosion_group)
                enemy.draw(self.display)

                if random.randrange(0, 4 * FPS) == 1:
                    enemy.shoot()

                if self.collide(enemy, self.player):
                    self.ships_collision(enemy)

                if enemy.y > HEIGHT:
                    self.enemies.remove(enemy)
                    self.player.lives -= 1

            self.generate_bonus()
            if self.bonus is not None:
                self.bonus.move()
                if self.collide(self.bonus, self.player):
                    self.bonus.apply(self.player)
                    self.bonus = None

            if len(self.explosion_group) == 0:
                self.level_changer()

            self.explosion_group.update()
            self.player.move(self.keys_pressed)
            self.player.move_bullets(self.enemies, self.explosion_group)
            self.draw_surfaces()
            self.reset_keys()

        self.reset_game()

    def ships_collision(self, enemy):
        """Resets some of player's attributes and generates explosion when ships collide."""
        self.player.lives -= 1
        self.player.reset_health()
        explosion = Explosion(enemy.get_centre_x(), enemy.get_centre_y(), 2)
        self.explosion_group.add(explosion)
        self.player.reset_position()
        self.enemies.remove(enemy)

    def check_events(self):
        """Checks for key presses."""
        pygame.event.pump()
        self.keys_pressed = pygame.key.get_pressed()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.running = False
                self.playing = False
                self.current_menu.run_display = False
                pygame.quit()
                exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RETURN:
                    self.start_key = True
                if event.key == pygame.K_ESCAPE:
                    self.escape_key = True
                if event.key == pygame.K_DOWN:
                    self.down_key = True
                if event.key == pygame.K_UP:
                    self.up_key = True

    def reset_keys(self):
        """Resets variables representing a state of particular keys."""
        self.up_key = False
        self.down_key = False
        self.start_key = False
        self.escape_key = False

    def draw_text(self, text, size, x, y):
        """Draws a given text of a given size at a given position."""
        font = pygame.font.Font(self.font_name, size)
        text_surface = font.render(text, True, WHITE)
        text_rectangle = text_surface.get_rect()
        text_rectangle.center = (x, y)
        self.display.blit(text_surface, text_rectangle)

    def draw_surfaces(self):
        """Draws elements of the game on the screen."""
        self.display.blit(self.background, (0, 0))
        self.window.blit(self.display, (0, 0))

        lives_label, lives_label_position = self.player.remaining_lives()
        self.display.blit(lives_label, lives_label_position)
        self.window.blit(self.display, (0, 0))

        self._healthbar(self.display)

        level_label, level_label_position = self._actual_level()
        self.display.blit(level_label, level_label_position)
        self.window.blit(self.display, (0, 0))

        score_label, score_label_position = self.player.actual_score()
        self.display.blit(score_label, score_label_position)
        self.window.blit(self.display, (0, 0))

        self.player.draw(self.display)
        self.window.blit(self.display, (0, 0))

        self.explosion_group.draw(self.display)

        if self.bonus is not None:
            if self.bonus.is_off_screen():
                self.bonus = None
            else:
                self.bonus.move()
                self.bonus.draw(self.display)
                self.window.blit(self.display, (0, 0))

        for enemy in self.enemies:
            enemy.draw(self.display)
            self.window.blit(self.display, (0, 0))

        pygame.display.update()

    def create_player(self):
        """Creates a new player."""
        self.player = Player(WIDTH // 2 - 25, HEIGHT - 80)

    def create_enemies(self):
        """Creates enemies and adds them to the self.enemies list."""
        if len(self.enemies) == 0:
            enemy_width = Enemy.ENEMY_TYPE[self.level][0].get_width()
            for _ in range(self.enemies_horde_size):
                enemy = Enemy(
                    x=random.randrange(1, WIDTH - enemy_width),
                    y=random.randrange(-1200, -50),  # y position above the screen
                    enemy_type_no=self.level
                )
                self.enemies.append(enemy)

    def _healthbar(self, window):
        """Provides visual representation of player's health."""
        health_percentage = self.player.health / self.player.max_health

        pygame.draw.rect(window, RED, (5, 48, 132, 5))
        pygame.draw.rect(window, GREEN, (5, 48, 132 * health_percentage, 5))

    def level_changer(self):
        """Changes game level based on progress in game (number of enemies killed)."""
        new_level = (self.player.killed_enemies // 10) + 1
        if new_level > self.level:
            self.level = new_level
            self.player.bullets.clear()
            self.enemies.clear()
            self.draw_surfaces()

            next_level_label = LARGE_FONT.render("next level", 1, WHITE)
            next_level_label_position = (
                (WIDTH // 2) - (next_level_label.get_width() // 2),
                (HEIGHT // 2) - (next_level_label.get_height() // 2)
            )
            self.display.blit(next_level_label, next_level_label_position)
            self.blit_screen()
            pygame.time.wait(1000)
            self.player.bullets = []

    def end_game_message(self):
        """Shows the end game message."""
        game_over_label = LARGE_FONT.render("game over!", 1, RED)
        game_over_label_position = (
            (WIDTH // 2) - (game_over_label.get_width() // 2),
            (HEIGHT // 2) - (game_over_label.get_height() // 2)
        )
        self.display.blit(game_over_label, game_over_label_position)
        self.blit_screen()
        pygame.time.wait(2000)

    def _actual_level(self):
        """Provides visual representation of actual level of the current game."""
        level_label = MAIN_FONT.render(f"level: {self.level}", 1, WHITE)
        label_position = (WIDTH - level_label.get_width() - 5, 5)
        return level_label, label_position

    def reset_game(self):
        """Resets variables to prepare to start a new game."""
        self.level = 1
        self.player = None
        self.enemies = []
        self.killed_enemies = 0
        self.playing = False

    def game_over(self):
        """Ends game."""
        self.end_game_message()
        self.reset_game()

    def generate_bonus(self):
        """Generates a random bonus item at a random frequency."""
        if random.randrange(0, 50 * FPS) == 1:
            bonus_type_no = random.randint(1, 2)
            bonus_velocity = random.randint(1, 4)
            bonus_y = random.randint(50, HEIGHT - 50)
            self.bonus = Bonus(bonus_type_no, bonus_velocity, bonus_y)
