import pygame
from constants import WIDTH, HEIGHT, BLACK


class Menu():
	"""Abstract class that is a blueprint for building a menu."""
	def __init__(self, game):
		self.game = game
		self.half_width, self.half_height = WIDTH // 2, HEIGHT // 2
		self.run_display = True
		self.cursor_rectangle = pygame.Rect(0, 0, 20, 20)
		self.offset = -110

	def draw_cursor(self):
		"""Draws a cursor, in form of letter "X", that points on an item in a menu."""
		self.game.draw_text("X", 20, self.cursor_rectangle.x, self.cursor_rectangle.y)


class MainMenu(Menu):
	"""Class represeting a Main Menu."""
	def __init__(self, game):
		super().__init__(game)
		self.state = "Start"
		self.start_x, self.start_y = self.half_width, self.half_height + 40
		self.options_x, self.options_y = self.half_width, self.half_height + 80
		self.credits_x, self.credits_y = self.half_width, self.half_height + 120
		self.exit_x, self.exit_y = self.half_width, self.half_height + 160
		self.cursor_rectangle.midtop = (self.start_x + self.offset, self.start_y)

	def display_menu(self):
		"""Displays Main Menu items."""
		self.run_display = True
		while self.run_display:
			self.game.check_events()
			self.check_input()
			self.game.display.fill(BLACK)
			self.game.draw_text("Galactic", 80, self.half_width, self.half_height - 320)
			self.game.draw_text("intruders", 80, self.half_width, self.half_height - 240)
			self.game.draw_text("Main Menu", 50, self.half_width, self.half_height - 40)
			self.game.draw_text("Start Game", 30, self.start_x, self.start_y)
			self.game.draw_text("options", 30, self.options_x, self.options_y)
			self.game.draw_text("Credits", 30, self.credits_x, self.credits_y)
			self.game.draw_text("exit", 30, self.exit_x, self.exit_y)
			self.draw_cursor()
			self.game.blit_screen()
			self.game.reset_keys()

	def move_cursor(self):
		"""Moves a cursor between all items within Main Menu."""
		if self.game.down_key:
			if self.state == "Start":
				self.cursor_rectangle.midtop = (self.options_x + self.offset, self.options_y)
				self.state = "Options"
			elif self.state == "Options":
				self.cursor_rectangle.midtop = (self.credits_x + self.offset, self.credits_y)
				self.state = "Credits"
			elif self.state == "Credits":
				self.cursor_rectangle.midtop = (self.exit_x + self.offset, self.exit_y)
				self.state = "Exit"
			elif self.state == "Exit":
				self.cursor_rectangle.midtop = (self.start_x + self.offset, self.start_y)
				self.state = "Start"
		elif self.game.up_key:
			if self.state == "Start":
				self.cursor_rectangle.midtop = (self.exit_x + self.offset, self.exit_y)
				self.state = "Exit"
			elif self.state == "Options":
				self.cursor_rectangle.midtop = (self.start_x + self.offset, self.start_y)
				self.state = "Start"
			elif self.state == "Credits":
				self.cursor_rectangle.midtop = (self.options_x + self.offset, self.options_y)
				self.state = "Options"
			elif self.state == "Exit":
				self.cursor_rectangle.midtop = (self.credits_x + self.offset, self.credits_y)
				self.state = "Credits"

	def check_input(self):
		"""Checks for user input (which Main Menu's item has been selected) and activate appropriate
		action within Main Menu."""
		self.move_cursor()
		if self.game.start_key:
			if self.state == "Start":
				self.game.create_player()
				self.game.playing = True
			elif self.state == "Options":
				self.game.current_menu = self.game.options_menu
			elif self.state == "Credits":
				self.game.current_menu = self.game.credits_menu
			elif self.state == "Exit":
				pygame.quit()
				exit()
		self.run_display = False


class OptionsMenu(Menu):
	"""Class represeting a submenu "Option Menu" that allows to manipulate some of a game settings."""
	def __init__(self, game):
		super().__init__(game)
		self.state = "Intro"
		self.intro_x, self.intro_y = self.half_width, self.half_height + 40
		self.controls_x, self.controls_y = self.half_width, self.half_height + 80
		self.cursor_rectangle.midtop = (self.intro_x + self.offset, self.intro_y)

	def display_menu(self):
		"""Displays Options Menu items."""
		self.run_display = True
		while self.run_display:
			self.game.check_events()
			self.check_input()
			self.game.display.fill(BLACK)
			self.game.draw_text("options", 50, self.half_width, self.half_height - 40)
			self.game.draw_text("intro", 30, self.intro_x, self.intro_y)
			self.game.draw_text("Controls", 30, self.controls_x, self.controls_y)
			self.draw_cursor()
			self.game.blit_screen()
			self.game.reset_keys()

	def check_input(self):
		"""Checks for user input (which Options Menu's item has been selected) and activate appropriate
		action within Options Menu."""
		if self.game.escape_key:
			self.game.current_menu = self.game.main_menu
			self.run_display = False
		elif self.game.up_key or self.game.down_key:
			if self.state == "Intro":
				self.state = "Controls"
				self.cursor_rectangle.midtop = (self.controls_x + self.offset, self.controls_y)
			elif self.state == "Controls":
				self.state = "Intro"
				self.cursor_rectangle.midtop = (self.intro_x + self.offset, self.intro_y)
		elif self.game.start_key:
			# TO-DO: Create a Controls Menu and other options
			pass


class CreditsMenu(Menu):
	"""Class representing a submenu "Credits Menu" that displays info about author."""
	def __init__(self, game):
		super().__init__(game)

	def display_menu(self):
		"""Displays Credits Menu items."""
		self.run_display = True
		while self.run_display:
			self.game.check_events()
			if self.game.start_key or self.game.escape_key:
				self.game.current_menu = self.game.main_menu
				self.run_display = False
			self.game.display.fill(BLACK)
			self.game.draw_text("Credits", 50, self.half_width, self.half_height - 40)
			self.game.draw_text("Made by", 30, self.half_width, self.half_height + 20)
			self.game.draw_text("iWannaBe.dev", 30, self.half_width, self.half_height + 60)
			self.game.blit_screen()
			self.game.reset_keys()