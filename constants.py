import pygame


pygame.font.init()


# Pygame settings
FPS = 60
WIDTH = 600
HEIGHT = 900

# Colours
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)

# Fonts
MAIN_FONT = pygame.font.Font("assets/Starjhol.ttf", 30)
LARGE_FONT = pygame.font.Font("assets/Starjhol.ttf", 70)

# Background
BACKGROUND = pygame.image.load("assets/background.jpg")

# Player
PLAYER_IMAGE = pygame.image.load("assets/spaceship.png")

# Enemies
ENEMY_1_IMAGE = pygame.image.load("assets/enemy1.png")
ENEMY_2_IMAGE = pygame.image.load("assets/enemy2.png")
ENEMY_3_IMAGE = pygame.image.load("assets/enemy3.png")
ENEMY_4_IMAGE = pygame.image.load("assets/enemy4.png")
ENEMY_5_IMAGE = pygame.image.load("assets/enemy5.png")
ENEMY_6_IMAGE = pygame.image.load("assets/enemy6.png")
ENEMY_7_IMAGE = pygame.image.load("assets/enemy7.png")
ENEMY_8_IMAGE = pygame.image.load("assets/enemy8.png")
ENEMY_9_IMAGE = pygame.image.load("assets/enemy9.png")
ENEMY_10_IMAGE = pygame.image.load("assets/enemy10.png")

# Bullets
PLAYER_BULLET_IMAGE = pygame.image.load("assets/player_bullet.png")
ENEMY_BULLET_IMAGE = pygame.image.load("assets/enemy_bullet.png")

# Bonus items
BONUS_LIFE_IMAGE = pygame.image.load("assets/heart.png")
BONUS_HEALTH_IMAGE = pygame.image.load("assets/medpak.png")