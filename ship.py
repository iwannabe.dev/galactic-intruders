import pygame
from constants import (
    FPS, WIDTH, HEIGHT, MAIN_FONT,  WHITE, PLAYER_IMAGE, PLAYER_BULLET_IMAGE,
    ENEMY_BULLET_IMAGE, ENEMY_1_IMAGE, ENEMY_2_IMAGE, ENEMY_3_IMAGE,
    ENEMY_4_IMAGE, ENEMY_5_IMAGE, ENEMY_6_IMAGE, ENEMY_7_IMAGE, ENEMY_8_IMAGE,
    ENEMY_9_IMAGE, ENEMY_10_IMAGE
)


class Ship:
    """"Abstract class that is a blueprint for a child classes (Enemy and Player)."""
    def __init__(self, x, y, health=100, shoot_interval_timeout=FPS/2):
        self.x = x
        self.y = y
        self.health = health
        self.ship_image = None
        self.bullet_image = None
        self.bullets = []
        self.shoot_interval_counter = 0
        self.shoot_interval_timeout = shoot_interval_timeout

    def draw(self, display):
        """Draws ship's image and bullets (if applicable) onto screen."""
        display.blit(self.ship_image, (self.x, self.y))
        for bullet in self.bullets:
            bullet.draw(display)

    def get_width(self):
        """Returns a width (in pixels) of a ship's image."""
        return self.ship_image.get_width()

    def get_height(self):
        """Returns a height (in pixels) of a ship's image."""
        return self.ship_image.get_height()

    def get_centre_x(self):
        """Returns an x-axis of a ship's centre position."""
        return self.x + (self.get_width() // 2)

    def get_centre_y(self):
        """Returns an y-axis of a ship's centre position."""
        return self.y + (self.get_height() // 2)

    def move_bullets(self, obj, explosion_group):
        """Moves fired bullets at a given interval, removes bullets that are outside of screen and
        applies appropriate action when bullet collide with another object."""
        self.shoot_interval()
        for bullet in self.bullets:
            bullet.move()
            if bullet.is_off_screen(HEIGHT):
                self.bullets.remove(bullet)
            elif bullet.is_collision(obj):
                obj.health -= 10
                explosion = Explosion(obj.get_centre_x(), obj.get_centre_y(), 1)
                explosion_group.add(explosion)
                self.bullets.remove(bullet)

    def shoot_interval(self):
        """Provides a mechanism to limit a frequency of shooting bullets."""
        if self.shoot_interval_counter >= self.shoot_interval_timeout:
            self.shoot_interval_counter = 0
        elif self.shoot_interval_counter > 0:
            self.shoot_interval_counter += 0.5

    def shoot(self):
        """Creates an appropriate object of Bullet class depending of ship's type."""
        if self.shoot_interval_counter == 0:
            bullet = Bullet(
                self.x + ((self.ship_image.get_width() - self.bullet_image.get_width()) // 2),
                self.y,
                self.bullet_image
            )

            # If a ship belongs to player, negate the bullet's velocity, so bullet can move the correct direction
            if self.ship_image == PLAYER_IMAGE:
                bullet.velocity = bullet.velocity * (-1)

            self.bullets.append(bullet)
            self.shoot_interval_counter = 1


class Player(Ship):
    """Class representing player's ship."""
    def __init__(self, x, y, health=100, velocity=2, lives=3):
        super().__init__(x, y, health)
        self.velocity = velocity
        self.lives = lives
        self.ship_image = PLAYER_IMAGE
        self.bullet_image = PLAYER_BULLET_IMAGE
        self.max_health = health
        self.score = 0
        self.killed_enemies = 0
        self.mask = pygame.mask.from_surface(self.ship_image)

    def move(self, keys_pressed):
        """Moves player's ship to a given direction (based on user input) and keeps that ship within screen area."""
        if keys_pressed[pygame.K_LEFT] and (self.x - self.velocity > 0):
            self.x -= self.velocity
        if keys_pressed[pygame.K_RIGHT] and (self.x + self.velocity + self.get_width() < WIDTH):
            self.x += self.velocity
        if keys_pressed[pygame.K_UP] and (self.y - self.velocity > 0):
            self.y -= self.velocity
        if keys_pressed[pygame.K_DOWN] and (self.y + self.velocity + self.get_height() < HEIGHT):
            self.y += self.velocity
        if keys_pressed[pygame.K_SPACE]:
            self.shoot()

    def move_bullets(self, objs, explosion_group):
        """Moves fired bullets at a given interval, removes bullets that are outside of screen and
        applies appropriate action when bullet collide with another object."""
        self.shoot_interval()
        for bullet in self.bullets.copy():
            bullet.move()
            if bullet.is_off_screen(HEIGHT):
                self.bullets.remove(bullet)

            for obj in objs.copy():
                if bullet.is_collision(obj):
                    self.score += 10
                    objs.remove(obj)
                    self.killed_enemies += 1
                    explosion = Explosion(obj.get_centre_x(), obj.get_centre_y(), 2)
                    explosion_group.add(explosion)
                    if self.bullets:
                        self.bullets.remove(bullet)
                    break

    def remaining_lives(self):
        """Provides a text label containig a current number of remaining lifes."""
        lives_label = MAIN_FONT.render(f"lives: {self.lives}", 1, WHITE)
        label_position = (5, 5)
        return lives_label, label_position

    def actual_score(self):
        """Provides a text label containig a current score."""
        score_label = MAIN_FONT.render(f"score: {self.score}", 1, WHITE)
        label_position = (WIDTH - score_label.get_width() - 5, 40)
        return score_label, label_position

    def reset_health(self):
        """Resets player's health."""
        self.health = self.max_health

    def add_life(self):
        """Adds life."""
        self.lives += 1

    def reset_position(self):
        """Resets player's position."""
        self.x = (WIDTH // 2) - 25
        self.y = HEIGHT - 80


class Enemy(Ship):
    """Class representing enemy's ship."""
    ENEMY_TYPE = {
        #  (image, velocity)
        1: (ENEMY_1_IMAGE, 1),
        2: (ENEMY_2_IMAGE, 1),
        3: (ENEMY_3_IMAGE, 1),
        4: (ENEMY_4_IMAGE, 2),
        5: (ENEMY_5_IMAGE, 2),
        6: (ENEMY_6_IMAGE, 2),
        7: (ENEMY_7_IMAGE, 3),
        8: (ENEMY_8_IMAGE, 3),
        9: (ENEMY_9_IMAGE, 3),
        10: (ENEMY_10_IMAGE, 3)
    }

    def __init__(self, x, y, enemy_type_no=1, health=100):
        super().__init__(x, y, health)
        self.ship_image, self.velocity = self.ENEMY_TYPE[enemy_type_no]
        self.bullet_image = ENEMY_BULLET_IMAGE
        self.mask = pygame.mask.from_surface(self.ship_image)

    def move(self):
        """Moves enemy's ship down along the screen by a value of current ship's velocity."""
        self.y += self.velocity


class Bullet:
    """"Class representing a bullet."""
    def __init__(self, x, y, image, velocity=3):
        self.x = x
        self.y = y
        self.image = image
        self.velocity = velocity
        self.mask = pygame.mask.from_surface(self.image)

    def draw(self, display):
        """Draws a bullet onto screen."""
        display.blit(self.image, (self.x, self.y))

    def move(self):
        """Moves a bullet down along the screen by a value of bullet's velocity."""
        self.y += self.velocity

    def is_collision(self, obj):
        """Checks if an object collided with another object."""
        offset_x = obj.x - self.x
        offset_y = obj.y - self.y

        return self.mask.overlap(obj.mask, (offset_x, offset_y)) != None

    def is_off_screen(self, height):
        """Checks if bullet is still within screen area."""
        return (self.y >= height) or (self.y <= 0)


class Explosion(pygame.sprite.Sprite):
    """Class representing an animated sprite for explosion."""
    def __init__(self, x, y, size):
        self.images = []
        self.apply_correct_image(size)
        super().__init__()
        self.index = 0
        self.image = self.images[self.index]
        self.rect = self.image.get_rect()
        self.rect.center = [x, y]
        self.counter = 0

    def apply_correct_image(self, size):
        """Applies correct size images for explosion."""
        for number in range(1, 13):
            image = pygame.image.load(f"assets/explosion_{number}.png")
            if size == 1:
                image = pygame.transform.scale(image, (98, 95))
            if size == 2:
                image = pygame.transform.scale(image, (196, 190))
            self.images.append(image)
    def update(self):
        """Updates frames of explosion's animation."""
        explosion_speed = 3
        self.counter += 1

        if (self.index < len(self.images) - 1) and (self.counter >= explosion_speed):
            self.counter = 0
            self.index += 1
            self.image = self.images[self.index]

        if (self.index >= len(self.images) - 1) and (self.counter >= explosion_speed):
            self.kill()