import pygame
from constants import HEIGHT, BONUS_LIFE_IMAGE, BONUS_HEALTH_IMAGE, WIDTH


class Bonus():
    """Class representing a bonus - extra item that can increase player's life, health etc."""
    BONUS_TYPE = {
        #  (type, value, image)
        1: ("life", 1, BONUS_LIFE_IMAGE),
        2: ("health", 100, BONUS_HEALTH_IMAGE),
        # 3: ("ship_speed", 2),
        # 4: ("bullet_speed", 2)
    }
    def __init__(self, bonus_type_no, velocity, y, x=-50):
        self.x = x
        self.y = y
        self.velocity = velocity
        self.type, self.value, self.image = self.BONUS_TYPE[bonus_type_no]
        self.mask = pygame.mask.from_surface(self.image)

    def move(self):
        """Moves a bonus object across screen."""
        self.x += self.velocity

    def draw(self, display):
        """Draws a bonus image onto screen."""
        display.blit(self.image, (self.x, self.y))

    def is_off_screen(self):
        """Checks if bonus moved outside of screen area."""
        return self.x >= WIDTH

    def apply(self, player):
        """Applies a bonus to relevant player's attribute."""
        if self.type == "life":
            player.add_life()
        if self.type == "health":
            player.reset_health()