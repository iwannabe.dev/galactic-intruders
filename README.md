# Galactic Intruders

Galactic Intruders is a 2D space shooter game made in Python, using the Pygame library. It features animated graphics and allows players to control a spaceship to defend against a constant stream of enemy intruders. The objective of the game is to shoot down as many enemies as possible while avoiding their attacks.

![gameplay](screenshot_0.png){width=40%} ![gameplay2](screenshot_1.png){width=40%}

## Features

- Arrow keys for spaceship movement (up, down, left, right)
- Space key for shooting projectiles
- 3 lives for the player
- Each level includes 10 enemies
- Enemies move at varying speeds depending on current level

## Installation

1. Ensure you have Python 3.x installed on your system.
2. Install the Pygame library by running the following command in your terminal:

   ```shell
   pip install pygame
   ```

3. Clone the repository or download the source code.
4. Navigate to the project directory.

## Usage

To start the game, simply run the following command in your terminal:

```shell
python main.py
```

Once launched, you can control your spaceship using the arrow keys. Press the space key to shoot projectiles at the oncoming enemies. Try to avoid enemy shots.